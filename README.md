
Oauth2-proxy
===========

A Helm chart for oauth2-proxy


Using [oauth2-proxy](https://github.com/oauth2-proxy/oauth2-proxy), setup a proxy to our [OIDC server](https://www.middleware.vt.edu/sso/oidc.html).

## Certificate & DNS

This chart will generate a LetsEncrypt certificate and Ingress using the provided `certificate.commonName`. You are responsible for setting up DNS for this name.

## Developer Configuration

In order to use this with [gateway](https://gateway.login.vt.edu), you will need to establish an app in SIS's [Developer App Manager](https://developer.it.vt.edu/). Protocol should be **OIDC**. Note the OAuth2 Client ID and Secret from the **OAuth2** tab of your application configuration.

In the **OAuth2 Redirect URIs** section of the **OAuth2** tab, add a callback of `https://<common-name>/oauth2/callback`.

In the **Attribute Release** tab, ensure that `targetedMembership` and `mailPreferredAddress` are checked.

In the **Targeted Groups** tab, add a group prefix to be targeted.

## Create a sealed secret

You will need to create a Kubernetes Secret to pass the needed client id, secret and cookie secret to the helm chart.
Follow the directions to create a [Sealed Secret](https://docs.platform.it.vt.edu/guides/creating-sealed-secrets/), using a key/value pair for client id (from developer config above), client secret (also from developer config) and cookie secret (a value you designate that is 16 or 32 bytes long).

## Example 

```yaml
apiVersion: source.toolkit.fluxcd.io/v1
kind: HelmRepository
metadata:
  name: oauth2-proxy
spec:
  url: https://code.vt.edu/api/v4/projects/20357/packages/helm/stable
  interval: 1h
  secretRef: 
    name: my-helm-chart-secret
---
apiVersion: helm.toolkit.fluxcd.io/v2
kind: HelmRelease
metadata:
  name: oauth2-proxy
spec:
  targetNamespace: my-namespace
  serviceAccountName: flux
  chart:
    spec:
      sourceRef:
        kind: HelmRepository
        name: oauth2-proxy
      chart: oauth2-proxy
      version: 0.1.1
  install:
    remediation: 
      retries: 4
  upgrade:
    remediation:
      retries: 4
  interval: 1h
  values:
    certificate: 
      commonName: "oauth2-proxy.mydept.vt.edu" 

    containerArgs:
      cookieDomain: ".mydept.vt.edu" 
      redirectUrl: "https://oauth2-proxy.mydept.vt.edu/oauth2/callback" 
      whitelistDomain: "*.mydept.vt.edu" 
      secrets:
        clientId:
          kubeSecret: "oauth2-proxy-secret" 
          key: "client_id" 
        clientSecret:
          kubeSecret: "oauth2-proxy-secret" 
          key: "client_secret" 
        cookieSecret:
          kubeSecret: "oauth2-proxy-secret" 
          key: "cookie_secret" 
```

## Using the proxy

See Helm chart [oauth2-ingress](https://code.vt.edu/it-common-platform/tenant-support/helm-charts/oauth2-ingress).

## Development

When developing this chart, if any change is made to `values.yaml`, update the README by doing the following:

```
pip install frigate
frigate gen --no-credits . > ./README.md
git commit -am "Update README"; git push origin <my-branch>
```

## Configuration

The following table lists the configurable parameters of the Oauth2-proxy chart and their default values.

| Parameter                | Description             | Default        |
| ------------------------ | ----------------------- | -------------- |
| `imageVersion` | The image version to use | `"v7.5.0"` |
| `replicaCount` | Number of oauth2-proxy instances to run | `1` |
| `nameOverride` | Override the default chart name | `""` |
| `fullnameOverride` | Override the full name | `""` |
| `certificate.commonName` | **required** The DNS name for oath2-proxy (must be less than 64 bytes) | `""` |
| `certificate.alternateNames` | Any additional DNS names you need | `[]` |
| `containerArgs.cookieDomain` | Optional cookie domains to force cookies to | `".vt.edu"` |
| `containerArgs.redirectUrl` | **required** the OAuth Redirect URL. ie: "https://oauth2-proxy.my.domain/oauth2/callback" | `""` |
| `containerArgs.oidcIssuerUrl` | **required** OpenID Connect issuer URL | `"https://gateway.login.vt.edu"` |
| `containerArgs.oidcEmailClaim` | **required** which OIDC claim contains the user's email | `"email"` |
| `containerArgs.oidcGroupsClaim` | **required** which OIDC claim contains the user groups | `"targetedMembership"` |
| `containerArgs.scope` | **required** OAuth scope specification | `"openid targetedMembership email"` |
| `containerArgs.whitelistDomain` | **required** allowed domains for redirection after authentication. Prefix domain with a . or a *. to allow subdomains (eg .example.com, *.example.com) | `"*.vt.edu"` |
| `containerArgs.secrets.clientId.kubeSecret` | **required** Kubernetes secret containing the OAuth Client ID | `""` |
| `containerArgs.secrets.clientId.key` | **required** Key in `containerArgs.secrets.clientId.kubeSecret` | `""` |
| `containerArgs.secrets.clientSecret.kubeSecret` | **required** Kubernetes secret containing the OAuth Client Secret | `""` |
| `containerArgs.secrets.clientSecret.key` | **required** Key in `containerArgs.secrets.clientSecret.kubeSecret` | `""` |
| `containerArgs.secrets.cookieSecret.kubeSecret` | **required** Kubernetes secret containing the seed string for secure cookies | `""` |
| `containerArgs.secrets.cookieSecret.key` | **required** Key in `containerArgs.secrets.cookieSecret.kubeSecret` | `""` |





